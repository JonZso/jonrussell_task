const mysql = require('mysql'); 
const express = require('express');
const jwt = require('jsonwebtoken')
var app = express();
const bodyparser = require('body-parser');
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

var mysqlConnection = mysql.createConnection({
    host:'109.203.107.115',
    user: 'jonrusse_root',
    password: '7MdVfNgUxVGi',
    database: 'jonrusse_playstation',
    multipleStatements: true
});

mysqlConnection.connect((err)=>{
    if(!err)
    console.log('DB Connection Succeded')
    else
    console.log('DB Connection Failed. \nError: ' + JSON.stringify(err, undefined, 2));
})

app.listen(3000,()=>console.log('Express Server is running at port: 3000'))

//Get all users
app.get('/users',(req, res)=>{
    mysqlConnection.query('SELECT * FROM UserLibrary',(err, rows, fields)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err)
    })

});
// Gets a view of all the games dashboard
app.get('/gamesDashboard',(req, res)=>{
    mysqlConnection.query('SELECT * from GamesDashboard',(err, rows, fields)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    });
})

app.get('/gamesDashboard/:id',(req, res)=>{
    mysqlConnection.query('SELECT * from GamesDashboard where gamesid = ?',[req.params.id],(err, rows, fields)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    });
})

app.get('/gamesLibrary',(req, res)=>{
    mysqlConnection.query('SELECT * FROM UserLibrary', (err, rows, fields)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err)
    });
});

app.get('/gamesLibrary/:id',(req, res)=>{
    mysqlConnection.query('SELECT * FROM UserLibrary WHERE UserID = ?',[req.params.id], (err, rows, fields)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err)
    });
});

app.get('/users/:id',(req, res)=>{
    mysqlConnection.query('SELECT * from Users where usersid = ?',[req.params.id], (err, rows, fields)=>{
    if(!err)
    res.send(rows);
    else
    console.log(err);
    })
});

//Insert an employee
app.put('/users', (req, res)=>{
    var emp = req.body;
    var sql = "SET @UsersID = ?; SET @Email = ?; SET @IsActive = ?; SET @PhoneNumber = ?; SET @Username = ?; SET @Firstname = ?; SET @Lastname = ?; SET @UserstatusID = ?; SET @IsTestUser = ?; \
    CALL UserAddOrEdit(@UsersID, @Email,@IsActive,@PhoneNumber,@Username,@Firstname,@Lastname,@UserstatusID,@IsTestUser);";

    mysqlConnection.query(sql,[emp.UsersID, emp.Email, emp.IsActive, emp.PhoneNumber, emp.Username, emp.Firstname, emp.Lastname,emp.UserstatusID, emp.IsTestUser],(err, rows, fields)=>{
        if(!err)
        res.send(rows);
        else
        console.log(err);
    })
  
});


app.post('/api/login', (req, res) =>{
    var emp = req.body;
    var sql = "SELECT * from Users where UserName = ?;";
    mysqlConnection.query(sql,[emp.UserName],(err, rows,fields) =>
    {
        if(!err) {
        // res.send(rows);
            const user = {
                userid: rows[0]['UsersID'],
                username: rows[0]['UserName'],
                email: rows[0]['Email']
            }
            jwt.sign({user}, 'secreyKey', (err,token) =>{
                res.json({
                    token
                });
            });             
        }        
        else
        console.log(err)
    })
})


app.post('/jon', (req,res)=>{
    var emp = req.body;
    var sql = "SELECT * from Users where UserName = ?;";
    const test = req.headers['jon'];
    console.log(test)
    
    mysqlConnection.query(sql,[emp.UserName],(err, rows,fields) =>
    {
        if(!err) {
        res.send(rows);
          
        }        
        else
        console.log(err)
    })

})

app.post('/games/:id/:gameid', verifyToken, (req,res)=>{
    jwt.verify(req.token, 'secretkey', (err, authData) =>{
        if(err)
        res.sendStatus(403);
        else
        res.join({
            message: 'okok',
            authData
        });
    });
})

//VerifyToken
function verifyToken(req,res,next){
    //get auth header value
    const bearerHeader = req.headers['authorization'];

    //check if bearer is undefned
    if(typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(' ');

        const bearerToken = bearer[1];

        req.token = bearerToken;

        next();
    }else{
        res.sendStatus(403);
    }

}

