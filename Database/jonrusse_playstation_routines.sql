-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 109.203.107.115    Database: jonrusse_playstation
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `GamesDashboard`
--

DROP TABLE IF EXISTS `GamesDashboard`;
/*!50001 DROP VIEW IF EXISTS `GamesDashboard`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `GamesDashboard` AS SELECT 
 1 AS `GamesID`,
 1 AS `Name`,
 1 AS `Platform`,
 1 AS `Genre`,
 1 AS `ReleaseDate`,
 1 AS `PlayerCount`,
 1 AS `Publisher`,
 1 AS `FileName`,
 1 AS `Url`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `UserDashboard`
--

DROP TABLE IF EXISTS `UserDashboard`;
/*!50001 DROP VIEW IF EXISTS `UserDashboard`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `UserDashboard` AS SELECT 
 1 AS `UsersID`,
 1 AS `Email`,
 1 AS `IsActive`,
 1 AS `PhoneNumber`,
 1 AS `FirstName`,
 1 AS `LastName`,
 1 AS `User Status`,
 1 AS `IsTestUser`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `UserLibrary`
--

DROP TABLE IF EXISTS `UserLibrary`;
/*!50001 DROP VIEW IF EXISTS `UserLibrary`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `UserLibrary` AS SELECT 
 1 AS `LibraryID`,
 1 AS `GameID`,
 1 AS `Game`,
 1 AS `UserID`,
 1 AS `UserName`,
 1 AS `Status`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `GamesDashboard`
--

/*!50001 DROP VIEW IF EXISTS `GamesDashboard`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jonrusse_work`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `GamesDashboard` AS select `Games`.`GamesID` AS `GamesID`,`Games`.`Name` AS `Name`,`pl`.`Name` AS `Platform`,`g`.`Name` AS `Genre`,`Games`.`ReleaseDate` AS `ReleaseDate`,`Games`.`PlayerCount` AS `PlayerCount`,`pub`.`Name` AS `Publisher`,`b`.`FileName` AS `FileName`,`b`.`Url` AS `Url` from ((((`Games` join `Platforms` `pl` on((`pl`.`PlatformsID` = `Games`.`PlatformID`))) join `Genres` `g` on((`g`.`GenresID` = `Games`.`GenreID`))) join `Publishers` `pub` on((`pub`.`PublishersID` = `Games`.`PublisherID`))) join `BoxArt` `b` on((`b`.`BoxArtID` = `Games`.`BoxArtID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `UserDashboard`
--

/*!50001 DROP VIEW IF EXISTS `UserDashboard`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jonrusse_root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `UserDashboard` AS select `Users`.`UsersID` AS `UsersID`,`Users`.`Email` AS `Email`,(case when (`Users`.`IsActive` = 1) then 'True' else 'False' end) AS `IsActive`,`Users`.`PhoneNumber` AS `PhoneNumber`,`Users`.`FirstName` AS `FirstName`,`Users`.`LastName` AS `LastName`,`u`.`Value` AS `User Status`,(case when (`Users`.`IsTestUser` = 1) then 'True' else 'False' end) AS `IsTestUser` from (`Users` join `UserStatus` `u` on((`u`.`UserStatusID` = `Users`.`UserStatusID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `UserLibrary`
--

/*!50001 DROP VIEW IF EXISTS `UserLibrary`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jonrusse_root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `UserLibrary` AS select `Library`.`LibraryID` AS `LibraryID`,`g`.`GamesID` AS `GameID`,`g`.`Name` AS `Game`,`u`.`UsersID` AS `UserID`,`u`.`UserName` AS `UserName`,`l`.`Name` AS `Status` from (((`Library` join `Users` `u` on((`u`.`UsersID` = `Library`.`UsersID`))) join `Games` `g` on((`g`.`GamesID` = `Library`.`GamesID`))) join `LibraryStatus` `l` on((`l`.`StatusID` = `Library`.`StatusID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-15 23:50:50
