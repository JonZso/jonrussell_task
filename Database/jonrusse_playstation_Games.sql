-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 109.203.107.115    Database: jonrusse_playstation
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Games`
--

DROP TABLE IF EXISTS `Games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Games` (
  `GamesID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(128) DEFAULT NULL,
  `PlatformID` int(11) NOT NULL,
  `GenreID` int(11) NOT NULL,
  `ReleaseDate` datetime DEFAULT NULL,
  `PlayerCount` int(11) NOT NULL,
  `PublisherID` int(11) NOT NULL,
  `BoxArtID` int(11) NOT NULL,
  PRIMARY KEY (`GamesID`),
  UNIQUE KEY `Name` (`Name`),
  KEY `fk_PlatformID` (`PlatformID`),
  KEY `fk_GenreID` (`GenreID`),
  KEY `fk_PublisherID` (`PublisherID`),
  KEY `fk_BoxArtID` (`BoxArtID`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Games`
--

LOCK TABLES `Games` WRITE;
/*!40000 ALTER TABLE `Games` DISABLE KEYS */;
INSERT INTO `Games` (`GamesID`, `Name`, `PlatformID`, `GenreID`, `ReleaseDate`, `PlayerCount`, `PublisherID`, `BoxArtID`) VALUES (1,'GOD OF WAR 4',1,1,'0000-00-00 00:00:00',4,1,1),(2,'God Eater 3',1,2,'2019-02-08 00:00:00',0,2,1),(3,'F1 2019',1,3,'2019-06-28 00:00:00',2,4,1),(4,'FIFA 20',1,4,'2019-12-27 00:00:00',2,4,1),(5,'Conarium',1,5,'2019-02-12 00:00:00',1,5,1),(6,'Fairy Tail',1,6,'2020-03-19 00:00:00',2,6,1),(7,'Death Stranding',1,2,'2019-11-08 00:00:00',1,1,1),(8,'Horizon Zero Dawn',1,2,'2017-02-28 00:00:00',1,1,1),(9,'Just Dance 2020',0,7,'2019-11-05 00:00:00',4,7,1),(10,'Grim Fandango Remastered',1,8,'2015-01-27 00:00:00',1,8,1),(11,'Battlefield V',1,9,'2018-10-19 00:00:00',64,4,1),(12,'The Last of Us Remastered',1,10,'2014-07-29 00:00:00',1,1,1),(13,'The Last of Us: Left Behind',1,10,'2015-05-12 00:00:00',1,1,1),(14,'The Last of Us Part II',1,10,'2020-05-29 00:00:00',1,1,1),(15,'Lego The Hobbit	',1,10,'2014-04-08 00:00:00',2,9,1),(16,'Lego Jurassic World	',1,10,'2015-06-12 00:00:00',2,9,1),(17,'Lego Marvel Super Heroes',1,10,'2013-11-15 00:00:00',2,9,1),(18,'Lego Marvel Super Heroes 2',1,10,'2017-11-14 00:00:00',2,9,1),(19,'Lego Marvel\'s Avengers',1,10,'2016-01-16 00:00:00',2,9,1),(20,'Lego Movie Videogame',1,10,'2014-02-07 00:00:00',2,9,1),(21,'Lego Movie Videogame 2',1,10,'2019-02-26 00:00:00',2,9,1),(22,'Lego Ninjago Movie Video Game',1,10,'2017-09-22 00:00:00',2,9,1),(23,'Lego Star Wars: The Force Awakens',1,10,'2016-06-28 00:00:00',2,9,1),(24,'Life Is Strange',1,10,'2015-01-30 00:00:00',1,10,1),(25,'Life Is Strange 2',1,11,'2018-09-26 00:00:00',1,10,1),(26,'Life Is Strange: Before the Storm',1,10,'2017-08-31 00:00:00',1,10,1);
/*!40000 ALTER TABLE `Games` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-15 23:50:46
